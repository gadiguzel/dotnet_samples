﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloWorld
{
    public partial class FormHelloWorld : Form
    {
        public FormHelloWorld()
        {
            InitializeComponent();
        }

        private void buttonSayIt_Click(object sender, EventArgs e)
        {
            String wordToSay = textBoxWordToSay.Text.ToString();

            labelWordSaid.Text = wordToSay;
        }
    }
}

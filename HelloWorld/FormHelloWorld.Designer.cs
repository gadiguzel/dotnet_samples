﻿namespace HelloWorld
{
    partial class FormHelloWorld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxWordToSay = new System.Windows.Forms.TextBox();
            this.labelWordToSay = new System.Windows.Forms.Label();
            this.buttonSayIt = new System.Windows.Forms.Button();
            this.labelWordSaid = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxWordToSay
            // 
            this.textBoxWordToSay.Location = new System.Drawing.Point(102, 41);
            this.textBoxWordToSay.Name = "textBoxWordToSay";
            this.textBoxWordToSay.Size = new System.Drawing.Size(202, 22);
            this.textBoxWordToSay.TabIndex = 0;
            // 
            // labelWordToSay
            // 
            this.labelWordToSay.AutoSize = true;
            this.labelWordToSay.Location = new System.Drawing.Point(99, 21);
            this.labelWordToSay.Name = "labelWordToSay";
            this.labelWordToSay.Size = new System.Drawing.Size(169, 17);
            this.labelWordToSay.TabIndex = 1;
            this.labelWordToSay.Text = "Say What U Want To Say";
            // 
            // buttonSayIt
            // 
            this.buttonSayIt.Location = new System.Drawing.Point(102, 69);
            this.buttonSayIt.Name = "buttonSayIt";
            this.buttonSayIt.Size = new System.Drawing.Size(202, 30);
            this.buttonSayIt.TabIndex = 2;
            this.buttonSayIt.Text = "Say it...";
            this.buttonSayIt.UseVisualStyleBackColor = true;
            this.buttonSayIt.Click += new System.EventHandler(this.buttonSayIt_Click);
            // 
            // labelWordSaid
            // 
            this.labelWordSaid.AutoSize = true;
            this.labelWordSaid.Location = new System.Drawing.Point(107, 133);
            this.labelWordSaid.Name = "labelWordSaid";
            this.labelWordSaid.Size = new System.Drawing.Size(13, 17);
            this.labelWordSaid.TabIndex = 3;
            this.labelWordSaid.Text = "-";
            // 
            // FormHelloWorld
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(435, 197);
            this.Controls.Add(this.labelWordSaid);
            this.Controls.Add(this.buttonSayIt);
            this.Controls.Add(this.labelWordToSay);
            this.Controls.Add(this.textBoxWordToSay);
            this.Name = "FormHelloWorld";
            this.Text = "Hello World";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxWordToSay;
        private System.Windows.Forms.Label labelWordToSay;
        private System.Windows.Forms.Button buttonSayIt;
        private System.Windows.Forms.Label labelWordSaid;
    }
}

